<?php
    
//add styles
function esyoh_styles() { 	
	wp_register_style( 'esyoh-widget-css', 'https://www.esyoh.com/clients/css/widget_style.css', array(), '1.0.0' );    
}
add_action( 'wp_enqueue_scripts', 'esyoh_styles',999);      

 
    
//assign programs
function _widget_esy_assign_programs(){
   //set header
   $program_names=array(    
    "GES117"=>"Pharmacy Technician",
    "GES705"=>"HVACR Certified Technician",
    "GES793"=>"HVACR Certified Technician",
    "GES118"=>"Veterinary Assistant",
    "GES324"=>"AutoCAD with 3D",
    "GES101"=>"Dental Assistant",
    "GES124"=>"Medical Billing and Coding",
    "GES703"=>"Freight Broker/Agent Training",
    "GES419"=>"Human Resources Professional",
    "GES270"=>"Paralegal",
    "GES202"=>"Certified Bookkeeper",
    "GES251"=>"The Complete Project Manager with CAPM and PMP Prep",
    "GES131"=>"Child Development Associate Training",
    "GES254"=>"Professional Bookkeeping",
    "GES327"=>"CompTIA Certification Training",
    "GES204"=>"Certified Wedding Planner",
    "GES249"=>"Mastering Project Management with PMP Prep",
    "GES319"=>"Web Developer",
    "GES218"=>"Travel Agent Training",
    "GES125"=>"Physical Therapy Aide",
    "GES147"=>"Medical Assistant",
    "GES248"=>"Project Management Essentials with CAPM Prep",
    "GES515"=>"Residential Interior Designer",
    "GES245"=>"Administrative Professional",
    "GES275"=>"Lean Six Sigma Green Belt and Black Belt",
    "GES332"=>"CCNA: Routing and Switching",
    "GES217"=>"Technical Writing",
    "GES316"=>"Web Applications Developer",
    "GES215"=>"Six Sigma Black Belt",
    "GES219"=>"Chartered Tax Professional",
    "GES112"=>"Medical Terminology",
    "GES273"=>"Lean Six Sigma Green Belt",
    "GES146"=>"Certified Personal Trainer",
    "GES518"=>"Graphic Design",
    "GES126"=>"Optician Certification Training",
    "GES409"=>"Payroll Practice and Management",
    "GES248"=>"Cyber Security",
    "GES773"=>"Electrician",
    "GES766"=>"Welding Technician",
    "GES515"=>"Certified Residential Interior Designer",
    "GES788"=>"Plumber",
    "GES174"=>"Medical Transcription",
    "GES197"=>"Patient Care Technician",
    "GES375"=>"Full Stack Developer",
    "GES1010"=>"Phlebotomy Technician",
    "GES1011"=>"EKG Technician"                    
   );
   
   return $program_names;
}    
    

//zip widget shortcode
 add_shortcode('zip_widget_esy', '_zip_widget_esy');
 function _zip_widget_esy($atts, $content = null) {
    global $wpdb;
   
		extract(shortcode_atts(array(				
				'pc' => 'GES793',
        'header' => '',
        'header_intro' => '',
        'domain' => '' 
		), $atts));
    
    
  if($domain==''){
   $domain=implode(".",array_slice(explode(".",get_site_url()), -2, 2, true));
  } 
  
   //enqueue_style
   wp_enqueue_style( 'esyoh-widget-css' );

    
   //set header
   $program_names=_widget_esy_assign_programs();
   
   
   if(strlen($header)<5){
      $header='Search '.$program_names[$pc].' Programs';
   }
   
   if(strlen($header_intro)<5){
      $header_intro='Get information on '.$program_names[$pc].' programs by entering your zip code and request enrollment information.';
   }   
  
   
    $display='<div class="zip_widget_esy_container">';
      $display.='<form action="https://www.esyoh.com/search" method="get">';
        $display.='<div class="zip_widget_esy_title">'.$header.'</div>';
        $display.='<p>'.$header_intro.'</p>';
  
      //check if multiple programs
      $explode_pc=explode(",",str_replace(" ","",rtrim($pc,',')));
      if(count($explode_pc)>1){
        $display.='<div class="zip_widget_esy_search_box_contianer">';
          
          $display.='<select name="pc" required class="program_type">';
          $display.='<option value="">Select Program</option>';

          $options=array();
          foreach($explode_pc as $row => $value){
            $options[$program_names[$value]]='<option value="'.$value.'">'.$program_names[$value].'</option>';
          }
          ksort($options);
          $display.=implode("",$options);

          $display.='</select>';
        $display.='</div>';
        $display.='<div class="zip_widget_esy_search_box_contianer"><input type="text" placeholder="Enter Zip" name="z" inputmode="numeric" /> <input type="submit" value="Find Schools" rel="sponsored"></div>';

        


        //single program
      }else{       
        $display.='<div class="zip_widget_esy_search_box_contianer"><input type="text" placeholder="Enter Zip" name="z" inputmode="numeric" /> <input type="submit" value="Find Schools" rel="sponsored"></div>';
        $display.='<input type="hidden" name="pc" value="'.$pc.'">';
      }


       $display.='<input type="hidden" name="d" value="'.$domain.'">';


      $display.='</form>';    
    $display.='</div>';   
    $display.='<div class="esy_sponsored">Sponsored Search</div>';   
   
   
    return $display;
			
	} 
  
  
  //listings widget shortcode
 add_shortcode('listings_widget_esy', '_listings_widget_esy');
 function _listings_widget_esy($atts, $content = null) {
    
    global $wpdb;
   
		extract(shortcode_atts(array(				
				'pc' => 'GES793',
        'header' => '',
        'header_intro' => '',
        'domain' => '',
        'results' => '5'
		), $atts));
    
    $rand_id=rand(1000,400000);
    
    if($domain==''){
     $domain=implode(".",array_slice(explode(".",get_site_url()), -2, 2, true));
    } 

     //enqueue_style
    wp_enqueue_style( 'esyoh-widget-css' );

         
    //get program names
    $program_names=_widget_esy_assign_programs();         
         
    //set header       
       
       if(strlen($header)<5){
          $header='Search '.$program_names[$pc].' Programs';
       }
       
       if(strlen($header_intro)<5){
          $header_intro='Get information on '.$program_names[$pc].' programs by entering your zip code and request enrollment information.';
       }   
      
       
        $display='<div class="listings_widget_esy_zip_search_container">';
          $display.='<form action="https://www.esyoh.com/search" method="get">';
            $display.='<div class="listings_widget_esy_title">'.$header.'</div>';
            $display.='<p>'.$header_intro.'</p>';
             $display.='<div class="listings_widget_esy_search_box_contianer"><input type="text" placeholder="Enter Zip" name="z" inputmode="numeric"/> <input type="submit" value="Find Schools" rel="sponsored" onclick></div>';
             $display.='<input type="hidden" name="d" value="'.$domain.'">';
             $display.='<input type="hidden" name="pc" value="'.$pc.'">';
          $display.='</form>';
        $display.='</div>';      
          //get geolocate via ip
     
     
         $display.='<script type="text/javascript">
                     window.addEventListener("load", function () {
                      jQuery(document).ready(function($) {      
                       var jsonp_url = "https://www.esyoh.com/clients/listings?r='.$results.'&d='.$domain.'&pc='.$pc.'&callback=?";
                        $.getJSON(jsonp_url, function(data) {
                          $(\'#listings_widget_esy_container_'.$rand_id.'\').html(data.html);
                        }); 
                      });    
                    });
                    </script>
                    <div id="listings_widget_esy_container_'.$rand_id.'" class="listings_widget_esy_container"></div>';
      $display.='<div class="esy_sponsored">Sponsored Listings</div>';                    
 
       
        return $display;
  
  
  }      