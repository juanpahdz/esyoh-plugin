<?php
   /*
   Plugin Name: Esyoh, LLC Client Plugin
   Plugin URI: https://www.esyoh.com
   Description: Esyoh, LLC School Widget and Listings Wordpress Plugin | Usage: To include the zip widget use shortcode [zip_widget_esy pc="INSERT PROGRAM CODE"], listing widget [listings_widget_esy results="NUMBER RESULTS TO SHOW" pc="INSERT PROGRAM CODE"]. Multiple programs can be used by separating program codes by commas ",". Ex: [zip_widget_esy pc="PROGRAMCODE1,PROGRAMCODE2,..."]
   Version: 4.3
   Author: Esyoh, LLC
   Author URI: https://www.esyoh.com
   */

define( 'ESYOH_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'ESYOH_PLUGIN_VERSION', '4.3');

require_once( ESYOH_PLUGIN_DIR . 'class.esyoh-functions.php' );
require_once( ESYOH_PLUGIN_DIR . 'class.esyoh-updater.php' );