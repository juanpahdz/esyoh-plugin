<?php

$esyoh_plugin_info_url='https://www.esyoh.com/wordpress_plugin_updates/get-plugin-information.txt';
$esyoh_plugin_version_url='https://www.esyoh.com/wordpress_plugin_updates/check-latest-version.txt';
$esyoh_plugin_update_host='www.esyoh.com';

function esyoh_plugin_api( $false, $action, $args ) {
    global $esyoh_plugin_info_url;
    $plugin_slug = 'esyoh-plugin';

    if(empty($args->slug)|| $args->slug != $plugin_slug)
        return $false;
 
	$args=$esyoh_plugin_info_url;
 
    // Send request for detailed information
    $response = esyoh_plugin_api_request( $args );  
     
    if ($response){        
        $response->sections=(array)$response->sections[0];
        $response->compatibility=(array)$response->compatibility[0];
        $response->banners=(array)$response->banners[0];

        return $response;

    } else{
        //If update location is not available this code will execute
        $tempResponse = new stdClass();
        $tempResponse->name = "Esyoh Client Plugin";
        $tempResponse->slug = 'esyoh-plugin'; 
        $tempResponse->sections = array(
            'description' => 'Now you can easily add Esyoh\'s Widget/Listings to your website\'s pages and posts.',        
        ); 

        return $tempResponse;     
    } 
    
 
}
add_filter( 'plugins_api', 'esyoh_plugin_api', 10, 3 );

function esyoh_plugin_update( $transient ) {
    
    global $esyoh_plugin_version_url;
 
    if ( empty( $transient->checked ) )
        return $transient; 
   
    $plugin_path = 'esyoh-plugin/esyoh-plugin.php';    
   
 	$args=$esyoh_plugin_version_url;

    // Send request checking for an update
    $response = esyoh_plugin_api_request($args);

    if ($response){       

        // If there is a new version, modify the transient
        if( version_compare( $response->new_version, $transient->checked[$plugin_path], '>' ) )
             $transient->response[$plugin_path] = $response;        
    }

    return $transient; 
}
add_filter( 'pre_set_site_transient_update_plugins', 'esyoh_plugin_update' );

function esyoh_plugin_api_request( $args ) {
 
    // Send request
 	$request = wp_remote_post($args);
 
    if ( is_wp_error( $request ) || 200 != wp_remote_retrieve_response_code( $request ) )
           return false;

    $response = json_decode(wp_remote_retrieve_body( $request ));
 
   if ( is_object( $response )&& !empty($response))        
        return $response;
    else   	 
        return false;
}

//Whitelist custom host
function allow_esyoh_custom_host( $allow, $host, $url ) {
    
    global $esyoh_plugin_update_host;
    
    if ($esyoh_plugin_update_host){        
        if ( $host == $esyoh_plugin_update_host)
            $allow = true;
        return $allow;
    }  
}
add_filter( 'http_request_host_is_external', 'allow_esyoh_custom_host', 10, 3 );
